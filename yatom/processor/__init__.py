# -*- coding: utf-8 -*-

from . import xml
from . import html

XMLProcessor = xml.XMLProcessor
HTMLProcessor = html.HTMLProcessor
