# -*- coding: utf-8 -*-

__version__ = '0.0.4'

import yatom.processor

XMLProcessor = yatom.processor.XMLProcessor
HTMLProcessor = yatom.processor.HTMLProcessor
